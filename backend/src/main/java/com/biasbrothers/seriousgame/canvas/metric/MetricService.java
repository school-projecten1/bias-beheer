package com.biasbrothers.seriousgame.canvas.metric;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MetricService {
    private final MetricRepository metricRepository;

    @Autowired
    public MetricService(MetricRepository metricRepository) {
        this.metricRepository = metricRepository;
    }

    public List<Metric> getAll() {
        return metricRepository.findAll();
    }

    public Metric getById(Long id) {
        return metricRepository.findById(id).orElseThrow(() -> new IllegalStateException(
                "metric with id " + id + " does not exist"));
    }

    public Metric add(Metric metric) {
        Metric newMetric = metric.clone();
        return metricRepository.save(newMetric);
    }

    public String delete(Long id) {
        metricRepository.deleteById(id);
        return "SUCCESS";
    }

    @Transactional
    public Metric update(Metric metric) {
        Metric newMetric = metricRepository.findById(metric.getId()).orElseThrow(() -> new IllegalStateException(
                "metric with id " + metric.getId() + " does not exist"));

        newMetric.setName(metric.getName());

        return metricRepository.save(newMetric);
    }
}
