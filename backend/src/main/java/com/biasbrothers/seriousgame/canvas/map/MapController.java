package com.biasbrothers.seriousgame.canvas.map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/map")
public class MapController {
    private final MapService mapService;

    @Autowired
    public MapController(MapService mapService) {
        this.mapService = mapService;
    }

    @CrossOrigin
    @GetMapping
    public List<Map> getAll() {
        return mapService.getAll();
    }

    @CrossOrigin
    @GetMapping(path = "/")
    public Map getById(@RequestParam Long id) {
        return mapService.getById(id);
    }

    @CrossOrigin
    @PostMapping(path = "/add")
    public ResponseEntity<Map> add(@RequestBody Map map) {
        Map mapTest = mapService.add(map);

        return new ResponseEntity<>(mapTest, HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        mapService.delete(id);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PutMapping(path = "/update/")
    public ResponseEntity<Map> update(@RequestBody Map map) {
        Map mapTest = mapService.update(map);

        return new ResponseEntity<>(mapTest, HttpStatus.ACCEPTED);
    }
}
