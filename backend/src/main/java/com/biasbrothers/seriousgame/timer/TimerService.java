package com.biasbrothers.seriousgame.timer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TimerService {
    private final TimerRepository timerRepository;

    @Autowired
    public TimerService(TimerRepository timerRepository) {
        this.timerRepository = timerRepository;
    }

    public List<Timer> getAll() {
        return timerRepository.findAll();
    }

    public Timer getById(Long id) {
        return timerRepository.findById(id).orElseThrow(() -> new IllegalStateException(
                "timer with id " + id + " does not exist"));
    }

    public Timer add(Timer timer) {
        Timer newTimer = timer.clone();
        return timerRepository.save(newTimer);
    }

    public String delete(Long id) {
        timerRepository.deleteById(id);
        return "SUCCESS";
    }

    @Transactional
    public Timer update(Timer timer) {
        Timer newTimer = timerRepository.findById(timer.getId()).orElseThrow(() -> new IllegalStateException(
                "timer with id " + timer.getId() + " does not exist"));

        newTimer.setName(timer.getName());

        return timerRepository.save(newTimer);
    }
}