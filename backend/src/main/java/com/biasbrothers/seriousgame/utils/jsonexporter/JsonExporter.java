package com.biasbrothers.seriousgame.utils.jsonexporter;

import com.biasbrothers.seriousgame.round.Round;

public interface JsonExporter {
    String exportJson(Round round);
}