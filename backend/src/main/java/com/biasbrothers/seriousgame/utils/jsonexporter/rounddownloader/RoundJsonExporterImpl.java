package com.biasbrothers.seriousgame.utils.jsonexporter.rounddownloader;

import com.biasbrothers.seriousgame.round.Round;
import com.biasbrothers.seriousgame.utils.jsonexporter.JsonExporter;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

@Service
public class RoundJsonExporterImpl implements JsonExporter {
    @Override
    public String exportJson(Round round) {
        Gson gson = new Gson();
        return gson.toJson(round);
    }
}
