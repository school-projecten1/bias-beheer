package com.biasbrothers.seriousgame.scenario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/scenario")
public class ScenarioController {
    private final ScenarioService scenarioService;

    @Autowired
    public ScenarioController(ScenarioService scenarioService) {
        this.scenarioService = scenarioService;
    }

    @CrossOrigin
    @GetMapping
    public List<Scenario> getAll() {
        return scenarioService.getAll();
    }

    @CrossOrigin
    @GetMapping(path = "/")
    public Scenario getById(@RequestParam Long id) {
        return scenarioService.getById(id);
    }

    @CrossOrigin
    @PostMapping(path = "/add")
    public ResponseEntity<Scenario> add(@RequestBody Scenario scenario) {
        Scenario scenarioTest = scenarioService.add(scenario);

        return new ResponseEntity<>(scenarioTest, HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        scenarioService.delete(id);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PutMapping(path = "/update/")
    public ResponseEntity<Scenario> update(@RequestBody Scenario scenario) {
        Scenario scenarioTest = scenarioService.update(scenario);

        return new ResponseEntity<>(scenarioTest, HttpStatus.ACCEPTED);
    }
}
