package com.biasbrothers.seriousgame.newsarticle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class NewsArticleService {
    private final NewsArticleRepository newsArticleRepository;

    @Autowired
    public NewsArticleService(NewsArticleRepository newsArticleRepository) {
        this.newsArticleRepository = newsArticleRepository;
    }

    public List<NewsArticle> getAll() {
        return newsArticleRepository.findAll();
    }

    public NewsArticle getById(Long id) {
        return newsArticleRepository.findById(id).orElseThrow(() -> new IllegalArgumentException(
                "News article with id " + id + " does not exist"));
    }

    public NewsArticle add(NewsArticle newsArticle) {
        NewsArticle newNewsArticle = newsArticle.clone();
        return newsArticleRepository.save(newNewsArticle);
    }

    public String delete(Long id) {
        newsArticleRepository.deleteById(id);
        return "SUCCESS";
    }

    @Transactional
    public NewsArticle update(NewsArticle newsArticle) {
        NewsArticle newNewsArticle = newsArticleRepository.findById(newsArticle.getId()).orElseThrow(() -> new IllegalArgumentException(
                "News article with id " + newsArticle.getId() + " does not exist"));

        newNewsArticle.setTitle(newsArticle.getTitle());
        newNewsArticle.setMessage(newsArticle.getMessage());
        newNewsArticle.setSource(newsArticle.getSource());
        newNewsArticle.setPopUp(newsArticle.getPopUp());

        return newsArticleRepository.save(newNewsArticle);
    }
}
