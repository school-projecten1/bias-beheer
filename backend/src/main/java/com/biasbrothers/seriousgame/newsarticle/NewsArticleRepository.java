package com.biasbrothers.seriousgame.newsarticle;

import com.biasbrothers.seriousgame.bias.Bias;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@EnableJpaRepositories
public interface NewsArticleRepository extends JpaRepository<NewsArticle, Long> {
}
