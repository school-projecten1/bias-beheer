package com.biasbrothers.seriousgame.bias;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bias")
public class BiasController {
    private final BiasService biasService;

    @Autowired
    public BiasController(BiasService biasService) {
        this.biasService = biasService;
    }

    @CrossOrigin
    @GetMapping
    public List<Bias> getAll() {
        return biasService.getAll();
    }

    @CrossOrigin
    @GetMapping(path = "/")
    public Bias getById(@RequestParam Long id) {
        return biasService.getById(id);
    }

    @CrossOrigin
    @PostMapping(path = "/add")
    public ResponseEntity<Bias> add(@RequestBody Bias bias) {
        Bias biasTest = biasService.add(bias);

        return new ResponseEntity<>(biasTest, HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {

        biasService.delete(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PutMapping(path = "/update/")
    public ResponseEntity<Bias> update(@RequestBody Bias bias) {
        Bias biasTest = biasService.update(bias);

        return new ResponseEntity<>(biasTest, HttpStatus.ACCEPTED);
    }
}
