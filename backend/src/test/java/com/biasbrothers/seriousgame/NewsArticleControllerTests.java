package com.biasbrothers.seriousgame;

import com.biasbrothers.seriousgame.newsarticle.NewsArticle;
import com.biasbrothers.seriousgame.newsarticle.NewsArticleController;
import com.biasbrothers.seriousgame.newsarticle.NewsArticleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = NewsArticleController.class)
public class NewsArticleControllerTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private NewsArticleService newsArticleService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findNewsArticleByIdTest() throws Exception {
        NewsArticle newsArticle = new NewsArticle(
                "Experts maken zich zorgen over stijgende aantal besmettingen Olifantengriep",
                "Veel blijft nog onbekend over de onlangs ontdekte Olifantengriep. Vooralsnog lijkt Noord-Kropslavië het enige land dat getroffen is. Inzichten in de verspreiding en werking van het virus blijven nog uit. Authoriteiten in Degressia zijn begonnen met de eerste maatregelen om het land voor te bereiden op een uitbraak.",
                "Degressia Dagblad",
                false
                );
        Mockito.doReturn(newsArticle).when(newsArticleService).getById(1L);

        final String expectedResponseContent = objectMapper.writeValueAsString(newsArticle);

        this.mvc.perform(get("/news_article/?id=1"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseContent));

        verify(newsArticleService).getById(1L);
    }

    @Test
    public void addNewsArticleTest() throws Exception {
        NewsArticle newsArticle = new NewsArticle("add", "add", "add", true);

        Mockito.when(newsArticleService.add(Mockito.any(NewsArticle.class))).thenReturn(newsArticle);

        final String expectedResponseContent = objectMapper.writeValueAsString(newsArticle);

        this.mvc.perform(post("/news_article/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedResponseContent));
    }

    @Test
    public void deleteNewsArticleTest() throws Exception {
        Mockito.when(newsArticleService.delete(1L)).thenReturn("SUCCESS");
        mvc.perform(delete("/news_article/delete/{id}", 1))
                .andExpect(status().isAccepted());
    }

    @Test
    public void updateNewsArticleTest() throws Exception {
        NewsArticle newsArticle = new NewsArticle("update", "update", "update", true);

        Mockito.when(newsArticleService.update(Mockito.any(NewsArticle.class))).thenReturn(newsArticle);

        final String expectedResponseContent = objectMapper.writeValueAsString(newsArticle);

        this.mvc.perform(put("/news_article/update/", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(content().json(expectedResponseContent));
    }
}
