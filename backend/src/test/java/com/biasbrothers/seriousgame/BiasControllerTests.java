package com.biasbrothers.seriousgame;

import com.biasbrothers.seriousgame.bias.Bias;
import com.biasbrothers.seriousgame.bias.BiasController;
import com.biasbrothers.seriousgame.bias.BiasService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = BiasController.class)
public class BiasControllerTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private BiasService biasService; // mock the repository

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findBiasByIdTest() throws Exception {
        Bias bias = new Bias(
                "Decoy effect",
                "In de keuze tussen twee producten zal er een voorkeur optreden voor een van de twee zodra je een er een optie tussen plaatst die (asymmetrisch) dichter bij een van de opties ligt.",
                "In de bioscoop verkoopt men kleine, medium en grote popcorn voor €2.50, €3,50 en €4,00 respectievelijk.");
        Mockito.doReturn(bias).when(biasService).getById(1L);

        final String expectedResponseContent = objectMapper.writeValueAsString(bias);

        this.mvc.perform(get("/bias/?id=1"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseContent));

        verify(biasService).getById(1L);
    }

    @Test
    public void addBiasTest() throws Exception {
        Bias bias = new Bias("add", "add", "add");

        Mockito.when(biasService.add(Mockito.any(Bias.class))).thenReturn(bias);

        final String expectedResponseContent = objectMapper.writeValueAsString(bias);

        this.mvc.perform(post("/bias/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedResponseContent));
    }

    @Test
    public void deleteBiasTest() throws Exception {
        Mockito.when(biasService.delete(1L)).thenReturn("SUCCESS");
        mvc.perform(delete("/bias/delete/{id}", 1))
                .andExpect(status().isAccepted());
    }

    @Test
    public void updateBiasTest() throws Exception {
        Bias bias = new Bias("update", "update", "update");
        Mockito.when(biasService.update(Mockito.any(Bias.class))).thenReturn(bias);

        final String expectedResponseContent = objectMapper.writeValueAsString(bias);

        this.mvc.perform(put("/bias/update/", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(content().json(expectedResponseContent));
    }
}
