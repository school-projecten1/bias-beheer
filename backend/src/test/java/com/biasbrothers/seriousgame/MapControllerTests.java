package com.biasbrothers.seriousgame;

import com.biasbrothers.seriousgame.canvas.map.Map;
import com.biasbrothers.seriousgame.canvas.map.MapController;
import com.biasbrothers.seriousgame.canvas.map.MapService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = MapController.class)
public class MapControllerTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private MapService mapService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findMapsByIdTest() throws Exception {
        Map map = new Map(
                "G1R1C3",
                "https://drive.google.com/file/d/105fIMab65FnR2laOn2aVwYDKALx4_2PA/preview");
        Mockito.doReturn(map).when(mapService).getById(1L);

        final String expectedResponseContent = objectMapper.writeValueAsString(map);

        this.mvc.perform(get("/map/?id=1"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseContent));

        verify(mapService).getById(1L);
    }

    @Test
    public void addMapTest() throws Exception {
        Map map = new Map("add", "add");

        Mockito.when(mapService.add(Mockito.any(Map.class))).thenReturn(map);

        final String expectedResponseContent = objectMapper.writeValueAsString(map);

        this.mvc.perform(post("/map/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedResponseContent));
    }

    @Test
    public void deleteMapTest() throws Exception {
        Mockito.when(mapService.delete(1L)).thenReturn("SUCCES");
        mvc.perform(delete("/map/delete/{id}", 1))
                .andExpect(status().isAccepted());
    }

    @Test
    public void updateMapTest() throws Exception {
        Map map = new Map("update", "update");

        Mockito.when(mapService.update(Mockito.any(Map.class))).thenReturn(map);

        final String expectedResponseContent = objectMapper.writeValueAsString(map);

        this.mvc.perform(put("/map/update/", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                        .andExpect(status().isAccepted())
                        .andExpect(content().json(expectedResponseContent));
    }
}
