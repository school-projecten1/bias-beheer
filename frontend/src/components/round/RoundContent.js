import { useState, useEffect } from "react";
import Backdrop from "../modal/Backdrop";
import checkModal from "../../Functions/checkModal";
import RoundForm from "./RoundForm";

const RoundContent = ({ roundContent, fetchAllRounds }) => {
    const [biasDropdown, setBiasDropdown] = useState([]);
    const [scenarioDropdown, setScenarioDropdown] = useState([]);
    const [timerDropdown, setTimerDropdown] = useState([]);
    const [canvasDropdown, setCanvasDropdown] = useState([]);
    const [updatedRound, setUpdatedRound] = useState(null);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [modalSaveOpen, setModalSaveOpen] = useState(false);

    useEffect(() => {
        fetchAllBiases();
        fetchAllScenarios();
        fetchAllTimers();
        fetchAllCanvas();
        setUpdatedRound(roundContent);
    }, [roundContent]);

    const fetchAllBiases = async () => {
        const res = await fetch('http://localhost:8080/api/bias');
        const data = await res.json();
        setBiasDropdown(data);
    }

    const fetchAllScenarios = async () => {
        const res = await fetch('http://localhost:8080/api/scenario');
        const data = await res.json();
        setScenarioDropdown(data);
    }

    const fetchAllTimers = async () => {
        const res = await fetch('http://localhost:8080/api/timer');
        const data = await res.json();
        setTimerDropdown(data);
    }

    const fetchAllCanvas = async () => {
        const res = await fetch('http://localhost:8080/api/canvas');
        const data = await res.json();
        setCanvasDropdown(data);
    }

    const deleteRound = async () => {
        await fetch(`http://localhost:8080/api/round/delete/${roundContent.id}`, {
            method: 'DELETE'
        });
        fetchAllRounds();
    }

    const putUpdatedRound = async (e) => {
        e.preventDefault();
        setModalSaveOpen(true);
        if (modalSaveOpen) {
            await putRequest();
        }
    }

    const putRequest = async () => {
        await fetch(`http://localhost:8080/api/round/update/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedRound)
        });
        fetchAllRounds();
    }

    const handleBiasDropdown = (e, index) => {
        const updatedBiasQuestions = updatedRound.biasQuestions;
        biasDropdown.forEach(bias => {
            if (index === 1) {
                if (e.target.value === bias.name) {
                    updatedBiasQuestions[0].bias = bias;
                    setUpdatedRound({ ...updatedRound, biasQuestions: updatedBiasQuestions });
                }
            } else if (index === 2) {
                if (e.target.value === bias.name) {
                    updatedBiasQuestions[1].bias = bias;
                    setUpdatedRound({ ...updatedRound, biasQuestions: updatedBiasQuestions });
                }
            } else if (index === 3) {
                if (e.target.value === bias.name) {
                    updatedBiasQuestions[2].bias = bias;
                    setUpdatedRound({ ...updatedRound, biasQuestions: updatedBiasQuestions });
                }
            }
        });
    }

    const handleCanvasDropdown = (e, index) => {
        const updatedCanvases = updatedRound.canvases;
        canvasDropdown.forEach(canvas => {
            if (index === 1) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[0] = canvas;
                    setUpdatedRound({ ...updatedRound, canvases: updatedCanvases });
                }
            } else if (index === 2) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[1] = canvas;
                    setUpdatedRound({ ...updatedRound, canvases: updatedCanvases });
                }
            } else if (index === 3) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[2] = canvas;
                    setUpdatedRound({ ...updatedRound, canvases: updatedCanvases });
                }
            } else if (index === 4) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[3] = canvas;
                    setUpdatedRound({ ...updatedRound, canvases: updatedCanvases });
                }
            } else if (index === 5) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[4] = canvas;
                    setUpdatedRound({ ...updatedRound, canvases: updatedCanvases });
                }
            }
        });
    }

    const handleScenarioDropdown = (e) => {
        scenarioDropdown.forEach(scenario => {
            if (e.target.value === scenario.title) {
                setUpdatedRound({ ...updatedRound, scenario: scenario });
            }
        });
    }

    const handleTimerDropdown = (e) => {
        timerDropdown.forEach(timer => {
            if (e.target.value === timer.name) {
                setUpdatedRound({ ...updatedRound, timer: timer });
            }
        });
    }

    const exportJSON = async () => {
        closeModalHandler();
        var a = document.createElement('a');
        a.href = `http://localhost:8080/api/download_round/${updatedRound.id}`;
        a.setAttribute('download', 'file');
        a.click();
        a.remove();
        return false;
    }

    const closeModalHandler = () => {
        setModalIsOpen(false);
        setModalSaveOpen(false);
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    return (
        <div className="div-content">
            {updatedRound !== null && (
                <RoundForm postOrPut={putUpdatedRound} round={updatedRound} setRound={setUpdatedRound} biasDropdown={biasDropdown}
                    scenarioDropdown={scenarioDropdown} timerDropdown={timerDropdown} canvasDropdown={canvasDropdown}
                    handleBiasDropdown={handleBiasDropdown} handleCanvasDropdown={handleCanvasDropdown} handleScenarioDropdown={handleScenarioDropdown}
                    handleTimerDropdown={handleTimerDropdown} openModalHandler={openModalHandler} />
            )}
            {(modalIsOpen || modalSaveOpen) && <Backdrop cancelModal={closeModalHandler} />}
            {checkModal(modalIsOpen, modalSaveOpen, closeModalHandler, deleteRound, putUpdatedRound)}
            <div style={{marginTop: "20px"}} className="div-save-button"><button className="button-export" onClick={exportJSON}>JSON Export</button></div>
        </div>
    );
};

export default RoundContent;
