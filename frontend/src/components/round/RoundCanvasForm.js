import { useState, useEffect, useCallback } from "react";

const RoundCanvasForm = ({ canvas }) => {



    return (
        <>
            <form className="round-form">
                <select>
                    <option value="Hoi">Selecteer canvas</option>
                </select>
                <div className="textarea-rows textarea-rows2">
                    <label>Canvas naam</label>
                    <label>Punten</label>
                </div>
                <div className="textarea-rows textarea-rows2">
                    <textarea value={canvas.name} className="greyed-out" readOnly />
                    <textarea value={canvas.points} className="greyed-out" readOnly />
                </div>
                <label>Kaart</label>
                <textarea value={canvas.map.name} className="greyed-out" readOnly />
                <label>Metriek 1</label>
                <div className="canvas-form-row-values">
                    <textarea className="greyed-out" readOnly value={canvas.metrics[0].name} />
                    <textarea className="greyed-out" value={canvas.value1} readOnly />
                </div>
                <label>Metriek 2</label>
                <div className="canvas-form-row-values">
                    <textarea className="greyed-out" readOnly value={canvas.metrics[1].name} />
                    <textarea className="greyed-out" value={canvas.value2} readOnly />
                </div>
                <label>Metriek 3</label>
                <div className="canvas-form-row-values">
                    <textarea className="greyed-out" readOnly value={canvas.metrics[2].name} />
                    <textarea className="greyed-out" value={canvas.value3} readOnly />
                </div>
                <label>Nieuwsartikel 1</label>
                <textarea className="greyed-out" readOnly value={canvas.newsArticles[0].message} />
                <label>Nieuwsartikel 2</label>
                <textarea className="greyed-out" readOnly value={canvas.newsArticles[1].message} />
                {canvas.newsArticles.length > 2 && (
                    <>
                        <label>Nieuwsartikel 3</label>
                        <textarea className="greyed-out" readOnly value={canvas.newsArticles[2].message} />
                    </>
                )}
            </form>
        </>
    );
};

export default RoundCanvasForm;
