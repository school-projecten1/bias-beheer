
const RoundForm = ({ postOrPut, round, setRound, biasDropdown, scenarioDropdown, timerDropdown, canvasDropdown,
    handleBiasDropdown, handleCanvasDropdown, handleScenarioDropdown, handleTimerDropdown, openModalHandler }) => {
    return (
        <form onSubmit={postOrPut} className="roundContent-form">
            <label>Rondetitel</label>
            <textarea onChange={e => setRound({ ...round, title: e.target.value })} value={round.title}></textarea>
            <label>Rondenummer</label>
            <textarea onChange={e => setRound({ ...round, roundNumber: e.target.value })} value={round.roundNumber}></textarea>
            <div className="textarea-rows textarea-rows2">
                <label>Bias 1</label>
                <label>Punten</label>
            </div>
            <select onChange={e => handleBiasDropdown(e, 1)}>
                <option defaultValue="Kies een bias">Kies een bias</option>
                {biasDropdown.map(bias => <option key={bias.id} value={bias.name}>{bias.name}</option>)}
            </select>
            <div className="textarea-rows textarea-rows2">
                <textarea className="greyed-out" value={round.biasQuestions[0].bias.name} readOnly />
                <textarea className="greyed-out" value={round.biasQuestions[0].points} readOnly />
            </div>
            <div className="textarea-rows textarea-rows2">
                <label>Bias 2</label>
                <label>Punten</label>
            </div>
            <select onChange={e => handleBiasDropdown(e, 2)}>
                <option defaultValue="Kies een bias">Kies een bias</option>
                {biasDropdown.map(bias => <option key={bias.id} value={bias.name}>{bias.name}</option>)}
            </select>
            <div className="textarea-rows textarea-rows2">
                <textarea className="greyed-out" value={round.biasQuestions[1].bias.name} readOnly />
                <textarea className="greyed-out" value={round.biasQuestions[1].points} readOnly />
            </div>
            <div className="textarea-rows textarea-rows2">
                <label>Bias 3</label>
                <label>Punten</label>
            </div>
            <select onChange={e => handleBiasDropdown(e, 3)}>
                <option defaultValue="Kies een bias">Kies een bias</option>
                {biasDropdown.map(bias => <option key={bias.id} value={bias.name}>{bias.name}</option>)}
            </select>
            <div className="textarea-rows textarea-rows2">
                <textarea className="greyed-out" value={round.biasQuestions[2].bias.name} readOnly />
                <textarea className="greyed-out" value={round.biasQuestions[2].points} readOnly />
            </div>
            <label>Scenario</label>
            <select onChange={e => handleScenarioDropdown(e)}>
                <option defaultValue="Kies een scenario">Kies een scenario</option>
                {scenarioDropdown.map(scenario => <option key={scenario.id} value={scenario.title}>{scenario.title}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.scenario.title} />
            <label>Timer</label>
            <select onChange={e => handleTimerDropdown(e)}>
                <option defaultValue="Kies een timer">Kies een timer</option>
                {timerDropdown.map(timer => <option key={timer.id} value={timer.name}>{timer.name}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.timer.name} />
            <label>Canvas 1</label>
            <select onChange={e => handleCanvasDropdown(e, 1)}>
                <option defaultValue="Kies een canvas">Kies een canvas</option>
                {canvasDropdown.map(canvas => <option key={canvas.id} value={canvas.name}>{canvas.name}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.canvases[0].name}></textarea>
            <label>Canvas 2</label>
            <select onChange={e => handleCanvasDropdown(e, 2)}>
                <option defaultValue="Kies een canvas">Kies een canvas</option>
                {canvasDropdown.map(canvas => <option key={canvas.id} value={canvas.name}>{canvas.name}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.canvases[1].name}></textarea>
            <label>Canvas 3</label>
            <select onChange={e => handleCanvasDropdown(e, 3)}>
                <option defaultValue="Kies een canvas">Kies een canvas</option>
                {canvasDropdown.map(canvas => <option key={canvas.id} value={canvas.name}>{canvas.name}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.canvases[2].name}></textarea>
            <label>Canvas 4</label>
            <select onChange={e => handleCanvasDropdown(e, 4)}>
                <option defaultValue="Kies een canvas">Kies een canvas</option>
                {canvasDropdown.map(canvas => <option key={canvas.id} value={canvas.name}>{canvas.name}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.canvases[3].name}></textarea>
            <label>Canvas 5</label>
            <select onChange={e => handleCanvasDropdown(e, 5)}>
                <option defaultValue="Kies een canvas">Kies een canvas</option>
                {canvasDropdown.map(canvas => <option key={canvas.id} value={canvas.name}>{canvas.name}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.canvases[4].name}></textarea>
            <div className="div-save-button">
                <button className="button-delete" onClick={openModalHandler}>Verwijder ronde</button>
                <input className="button-save" type="submit" value="Opslaan" />
            </div>
        </form>
    );
};

export default RoundForm;
