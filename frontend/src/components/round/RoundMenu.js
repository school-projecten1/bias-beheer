import { useCallback, useEffect, useState } from "react";
import ButtonList from "../ButtonList";
import ModalMenu from "../modal/ModalMenu";
import Backdrop from "../modal/Backdrop"
import SearchIcon from '@mui/icons-material/Search';
import RoundContent from "./RoundContent";
import RoundAdd from "./RoundAdd";

const RoundMenu = () => {
    const [roundList, setRoundList] = useState([]);
    const [chosenIndex, setChosenIndex] = useState(0);
    const [roundContent, setRoundContent] = useState([]);
    const [listForButtons, setListForButtons] = useState([]);
    const [openModalMenu, setOpenModalMenu] = useState(false);
    const [searchInput, setSearchInput] = useState('');
    const [newRound, addRound] = useState(false);

    const fetchAllRounds = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/round');
        const data = await res.json();
        setRoundList(data);
        setButtons(data);
        setChosenIndex(false);
        addRound(false);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            fetchAllRounds();
        }
        fetchData();
    }, [fetchAllRounds]);

    const setButtons = (data) => {
        const buttonList = [];
        data.forEach(round => {
            const button = { id: round.id, name: `${round.roundNumber} ${round.title}` }
            buttonList.push(button);
        });
        setListForButtons(buttonList);
    }

    const handleRoundClick = (clickedRoundId) => {
        roundList.forEach(round => {
            if (round.id === clickedRoundId) {
                addRound(false);
                setRoundContent(round);
                setChosenIndex(clickedRoundId);
                setOpenModalMenu(false);
                setSearchInput('');
            }
        });
    }

    const clickAddRound = () => {
        setChosenIndex(false);
        addRound(true);
    }

    const cancelHandler = () => {
        setOpenModalMenu(false);
        setSearchInput('');
        setButtons(roundList);
    }

    const openMenu = () => {
        setOpenModalMenu(true);
    }

    const returnSearchList = (searchValue) => {
        setSearchInput(searchValue);
        if (searchInput !== '') {
            const filteredData = roundList.filter((item) => {
                return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
            })
            setButtons(filteredData);
        } else {
            setButtons(roundList);
        }
    }

    return (
        <div className="container-menu">
            <div className="btn-group">
                {roundList.length > 0 && <ButtonList list={listForButtons} handleItemClick={handleRoundClick} color={chosenIndex} />}
                <button onClick={clickAddRound} title="addBias">+</button>
            </div>
            <div className="container-content">
                {!newRound && !chosenIndex ? 'Selecteer een ronde of maak een nieuwe aan.' : ''}
                {chosenIndex && <RoundContent roundContent={roundContent} fetchAllRounds={fetchAllRounds} />}
                {newRound ? <RoundAdd fetchAllRounds={fetchAllRounds} /> : ''}
            </div>
            <button className="button-menu" onClick={openMenu} title="menu"><SearchIcon /></button>
            <div>
                <button className="button-add" onClick={clickAddRound}>+</button>
            </div>
            <div>
                {openModalMenu && <ModalMenu modalQuestion="Maak een keuze uit het menu" cancelHandler={cancelHandler} modalCancelText="Annuleren" handleClick={handleRoundClick} listForButtons={listForButtons} listForResults={(e) => returnSearchList(e.target.value)} />}
                {openModalMenu && <Backdrop cancelModal={cancelHandler} />}
            </div>
        </div>
    );
};

export default RoundMenu;
