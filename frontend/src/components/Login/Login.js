const Login = ({ handleSubmit, setUsername, username, setPassword, password }) => {

    return (
        <div className="login">
            <form onSubmit={handleSubmit} className="loginForm">
                <div className="form-inner">
                    <h2>Login</h2>
                    {/* {error !== "" ? <div className="error"></div> : ""} */}
                    <div className="form-group">
                        <label htmlFor="name">Naam:</label>
                        <input type="text" name="name" className="LoginName" onChange={({ target }) => setUsername(target.value)} value={username} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Wachtwoord:</label>
                        <input type="password" name="password" className="password" onChange={({ target }) => setPassword(target.value)} value={password} />
                    </div>
                    <div className="div-login">
                        <input type="submit" value="LOGIN" className="loginButton" />
                    </div>
                </div>
            </form>
        </div>

    )

};

export default Login;
