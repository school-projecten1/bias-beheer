import { useState, useEffect, useCallback } from "react";
import NewsArticleAdd from "./NewsArticleAdd";
import ButtonList from "../ButtonList";
import NewsArticleContent from "./NewsArticleContent";
import ModalMenu from "../modal/ModalMenu";
import Backdrop from "../modal/Backdrop";
import SearchIcon from '@mui/icons-material/Search';

const NewsArticleMenu = () => {
    const [newsArticleList, setNewsArticleList] = useState([]);
    const [newsArticleContent, setNewsArticleContent] = useState([]);
    const [chosenIndex, setChosenIndex] = useState(0);
    const [newNewsArticle, addNewsArticle] = useState(false);
    const [listForButtons, setListForButtons] = useState([]);
    const [openModalMenu, setOpenModalMenu] = useState(false);
    const [searchInput, setSearchInput] = useState('');

    const fetchAllNewsArticles = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/news_article');
        const data = await res.json();
        setNewsArticleList(data);
        setButtons(data);
        setChosenIndex(false);
        addNewsArticle(false);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            fetchAllNewsArticles();
        }
        fetchData();
    }, [fetchAllNewsArticles]);

    const handleNewsArticleClick = (clickedNewsArticleId) => {
        newsArticleList.forEach(newsArticle => {
            if (newsArticle.id === clickedNewsArticleId) {
                addNewsArticle(false);
                setNewsArticleContent(newsArticle);
                setChosenIndex(clickedNewsArticleId);
                setOpenModalMenu(false);
                setButtons(newsArticleList);
            }
        });
    }

    const clickAddNewNewsArticle = () => {
        setChosenIndex(false);
        addNewsArticle(true)
    }

    const setButtons = (data) => {
        const buttonList = [];
        data.forEach(newsArticle => {
            const button = { id: newsArticle.id, name: newsArticle.title }
            buttonList.push(button);
        });
        setListForButtons(buttonList);
    }

    const cancelHandler = () => {
        setOpenModalMenu(false);
        setButtons(newsArticleList);
    }

    const openMenu = () => {
        setOpenModalMenu(true);
    }

    const returnSearchList = (searchValue) => {
        setSearchInput(searchValue);
        if (searchInput !== '') {
            const filteredData = newsArticleList.filter((item) => {
                return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
            })
            setButtons(filteredData);
        } else {
            setButtons(newsArticleList);
        }
    }

    return (
        <div className="container-menu">
            <div className="btn-group-long">
                {newsArticleList.length > 0 && <ButtonList list={listForButtons} handleItemClick={handleNewsArticleClick} color={chosenIndex} />}
                <button title="addNewsArticle" onClick={clickAddNewNewsArticle}>+</button>
            </div>
            <div className="container-content">
                {!newNewsArticle && !chosenIndex ? 'Selecteer een nieuws artikel of maak een nieuwe aan.' : ''}
                {chosenIndex && <NewsArticleContent newsArticleContent={newsArticleContent} fetchAllNewsArticles={fetchAllNewsArticles} />}
                {newNewsArticle ? <NewsArticleAdd fetchAllNewsArticles={fetchAllNewsArticles} /> : ''}
            </div>
            <button className="button-menu" onClick={openMenu}><SearchIcon /></button>
            <div>
                <button className="button-add" title="menu" onClick={clickAddNewNewsArticle}>+</button>
            </div>
            <div>
                {openModalMenu && <ModalMenu modalQuestion="Maak een keuze uit het menu" cancelHandler={cancelHandler} modalCancelText="Annuleren" handleClick={handleNewsArticleClick} listForButtons={listForButtons} listForResults={(e) => returnSearchList(e.target.value)} />}
                {openModalMenu && <Backdrop cancelModal={cancelHandler} />}
            </div>
        </div>
    );
}

export default NewsArticleMenu;
