const Modal = ({ cancelHandler, confirmHandler, modalQuestion, modalCancelText, modalConfirmText }) => {

    return (
        <div className="modal">
            <p>{modalQuestion}</p>
            <div className="div-modal">
                <button className="button-cancel" onClick={cancelHandler}>{modalCancelText}</button>
                <button className="button-save" onClick={confirmHandler}>{modalConfirmText}</button>
            </div>
        </div>
    );
}

export default Modal;
