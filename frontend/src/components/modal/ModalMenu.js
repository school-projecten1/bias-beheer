import ButtonList from "../ButtonList";
import Searchbar from "./Searchbar";

const ModalMenu = ({ cancelHandler, modalQuestion, modalCancelText, handleClick, listForButtons, listForResults }) => {
    return (

        <div className="menuModal">
            <Searchbar searchForItems={listForResults} />
            <p id="menuId">{modalQuestion}</p>
            <div className="buttons-flexMenu">
                <ButtonList list={listForButtons} handleItemClick={handleClick} />
            </div>
            <div className="div-modal">
                <button className="button-cancelMenu" onClick={cancelHandler}>{modalCancelText}</button>
            </div>
        </div>

    )
}

export default ModalMenu
