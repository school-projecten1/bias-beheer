import { useState, useEffect } from "react";
import Backdrop from "../modal/Backdrop";
import checkModal from "../../Functions/checkModal";

const MapContent = ({ mapContent, fetchAllMaps }) => {
    const [name, setName] = useState('');
    const [url, setUrl] = useState('');
    const [modalIsOpen, setModalIsOpen] = useState(null);
    const [modalSaveOpen, setModalSaveOpen] = useState(null);

    useEffect(() => {
        setName(mapContent.name);
        setUrl(mapContent.url);
    }, [mapContent]);

    const putUpdatedMap = async (e) => {
        e.preventDefault();
        const updatedMap = {
            id: mapContent.id,
            name: name,
            url: url
        };
        await putRequest(updatedMap);
    }

    const putRequest = async (updatedMap) => {
        await fetch(`http://localhost:8080/api/map/update/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedMap)
        });
        setModalSaveOpen(true);
        if (modalSaveOpen) {
            fetchAllMaps(); // Refreshes the list and loads the buttons
        }

    }

    const deleteMap = async () => {
        await fetch(`http://localhost:8080/api/map/delete/${mapContent.id}`, {
            method: 'DELETE'
        });
        fetchAllMaps();
    }

    const closeModalHandler = () => {
        setModalIsOpen(false);
        setModalSaveOpen(false);
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    return (
        <div className="map-Content">
            <form className="mapContent-form"
                onSubmit={putUpdatedMap}>
                <label>Naam: </label>
                <textarea value={name} onChange={e => setName(e.target.value)} />
                <label>Image url: </label>
                <textarea value={url} onChange={e => setUrl(e.target.value)} />
                <label>Kaart voorbeeld: </label>
                <iframe className="text-area-maps" src={url} width="640" height="480" allow="autoplay" title={"preview"} />
                <div className="div-save-button">
                    <button className="button-delete" onClick={openModalHandler}>Verwijder Kaart</button>
                    <input className="button-save" type="submit" value="Opslaan" />
                </div>
            </form>
            {(modalIsOpen || modalSaveOpen) && <Backdrop cancelModal={closeModalHandler} />}
            {checkModal(modalIsOpen, modalSaveOpen, closeModalHandler, deleteMap, putUpdatedMap)}
        </div>
    )
}

export default MapContent;