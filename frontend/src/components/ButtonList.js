const ButtonList = ({ list, handleItemClick, color }) => {
    const listAlphabetically = list.sort((a, b) => a.name.localeCompare(b.name));

    return (
        <>
            {listAlphabetically.map((listItem) => (
                <button title="menuButton" key={listItem.id} onClick={() => handleItemClick(listItem.id)} id={color === listItem.id ? "button-clicked" : undefined}>{listItem.name}</button>
            ))}
        </>
    );
}

export default ButtonList;