import { useState, useEffect, useCallback } from "react";
import CanvasAdd from "./CanvasAdd";
import ButtonList from "../ButtonList";
import CanvasContent from "./CanvasContent";
import ModalMenu from "../modal/ModalMenu";
import Backdrop from "../modal/Backdrop"
import SearchIcon from '@mui/icons-material/Search';

const CanvasMenu = () => {
    const [canvasList, setCanvasList] = useState([]);
    const [canvasContent, setCanvasContent] = useState([]);
    const [chosenIndex, setChosenIndex] = useState(null);
    const [newCanvas, addCanvas] = useState(false);
    const [listForButtons, setListForButtons] = useState([]);
    const [openModalMenu, setOpenModalMenu] = useState(false);
    const [searchInput, setSearchInput] = useState('');
    const [mapDropdown, setMapDropdown] = useState([]);
    const [metricsDropdown, setMetricsDropdown] = useState([]);
    const [newsArticlesDropdown, setNewsArticlesDropdown] = useState([]);
    const [map, setMap] = useState('');
    const [newsArticles, setNewsArticles] = useState({
        newsArticle1: '',
        newsArticle2: '',
        newsArticle3: ''
    });
    const [metrics, setMetrics] = useState({
        metric1: '',
        metric2: '',
        metric3: ''
    });

    const fetchAllCanvas = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/canvas');
        const data = await res.json();
        setCanvasList(data);
        setListForButtons(data);
        setChosenIndex(false);
        addCanvas(false);
    }, []);

    const fetchMaps = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/map');
        const data = await res.json();
        setMapDropdown(data);
    }, []);

    const fetchMetrics = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/metric');
        const data = await res.json();
        setMetricsDropdown(data);
    }, []);

    const fetchNewsArticles = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/news_article');
        const data = await res.json();
        setNewsArticlesDropdown(data);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            fetchAllCanvas();
            fetchMaps();
            fetchMetrics();
            fetchNewsArticles();
        }
        fetchData();
    }, [fetchAllCanvas, fetchMaps, fetchMetrics, fetchNewsArticles]);

    const handleMapDropdown = (e) => {
        mapDropdown.forEach(map => {
            if (e.target.value === map.name) {
                setMap(map);
            }
        });
    }

    const handleMetricDropdown = (e, index) => {
        metricsDropdown.forEach(metric => {
            if (index === 1) {
                if (e.target.value === metric.name) {
                    setMetrics({ ...metrics, metric1: metric });
                }
            } else if (index === 2) {
                if (e.target.value === metric.name) {
                    setMetrics({ ...metrics, metric2: metric });
                }
            } else if (index === 3) {
                if (e.target.value === metric.name) {
                    setMetrics({ ...metrics, metric3: metric });
                }
            }
        });
    }

    const handleNewsArticleDropdown = (e, index) => {
        newsArticlesDropdown.forEach(article => {
            if (index === 1) {
                if (e.target.value === article.title) {
                    setNewsArticles({ ...newsArticles, newsArticle1: article });
                }
            } else if (index === 2) {
                if (e.target.value === article.title) {
                    setNewsArticles({ ...newsArticles, newsArticle2: article });
                }
            } else if (index === 3) {
                if (e.target.value === article.title) {
                    setNewsArticles({ ...newsArticles, newsArticle3: article });
                }
            }
        });
    }

    const clickAddCanvas = () => {
        setChosenIndex(false);
        addCanvas(true)
        setMap('');
        setMetrics({
            metric1: '',
            metric2: '',
            metric3: ''
        });
        setNewsArticles({
            newsArticle1: '',
            newsArticle2: '',
            newsArticle3: ''
        });
    }

    const handleCanvasClick = (clickedCanvasId) => {
        canvasList.forEach(canvas => {
            if (canvas.id === clickedCanvasId) {
                addCanvas(false);
                setCanvasContent(canvas);
                setChosenIndex(clickedCanvasId);
                setOpenModalMenu(false);
                setListForButtons(canvasList);
            }
        });
    }

    const cancelHandler = () => {
        setOpenModalMenu(false);
        setListForButtons(canvasList);
    }

    const openMenu = () => {
        setOpenModalMenu(true);
    }

    const returnFilteredData = (searchValue) => {
        setSearchInput(searchValue);
        if (searchInput !== '') {
            const filteredData = canvasList.filter((item) => {
                return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
            });
            setListForButtons(filteredData);
        } else {
            setListForButtons(canvasList);
        }
    }

    return (
        <div className="container-menu">
            <div className="btn-group">
                {canvasList.length > 0 && <ButtonList list={listForButtons} handleItemClick={handleCanvasClick} color={chosenIndex} />}
                <button title="addCanvas" onClick={clickAddCanvas}>+</button>
            </div>
            <div className="container-content">
                {!newCanvas && !chosenIndex ? 'Selecteer een canvas of maak een nieuwe aan.' : ''}
                {chosenIndex && <CanvasContent canvasContent={canvasContent} fetchAllCanvas={fetchAllCanvas}
                    map={map} metrics={metrics} setMetrics={setMetrics}
                    newsArticles={newsArticles} setNewsArticles={setNewsArticles}
                    handleMapDropdown={handleMapDropdown} handleMetricDropdown={handleMetricDropdown} handleNewsArticleDropdown={handleNewsArticleDropdown}
                    setMap={setMap} mapDropdown={mapDropdown} metricsDropdown={metricsDropdown} newsArticlesDropdown={newsArticlesDropdown} />}
                {newCanvas ? <CanvasAdd fetchAllCanvas={fetchAllCanvas}
                    map={map} metrics={metrics} setMetrics={setMetrics}
                    newsArticles={newsArticles} setNewsArticles={setNewsArticles}
                    handleMapDropdown={handleMapDropdown} handleMetricDropdown={handleMetricDropdown} handleNewsArticleDropdown={handleNewsArticleDropdown}
                    setMap={setMap} mapDropdown={mapDropdown} metricsDropdown={metricsDropdown} newsArticlesDropdown={newsArticlesDropdown} /> : ''}
            </div>
            <button title="menu" className="button-menu" onClick={openMenu}><SearchIcon /></button>
            <div>
                <button className="button-add" onClick={clickAddCanvas}>+</button>
            </div>
            <div>
                {openModalMenu && <ModalMenu modalQuestion="Maak een keuze uit het menu" cancelHandler={cancelHandler} modalCancelText="Annuleren" handleClick={handleCanvasClick} listForButtons={listForButtons} listForResults={(e) => returnFilteredData(e.target.value)} />}
                {openModalMenu && <Backdrop cancelModal={cancelHandler} />}
            </div>
        </div>
    );
}

export default CanvasMenu;
