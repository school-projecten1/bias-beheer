import '@testing-library/jest-dom/extend-expect';
import { render, screen, fireEvent } from '@testing-library/react';
import CanvasMenu from './CanvasMenu';

// Kijkt of de add knop goed wordt ingeladen
it('Check if addBias renders correct', () => {
    const { queryByTitle } = render(<CanvasMenu />);
    const btn = queryByTitle("addCanvas");
    expect(btn).toBeTruthy();
})

// Checkt of de add button het juiste component inlaad
describe('Check the add button', () => {
    it('onClick', () => {
        const { queryByTitle } = render(<CanvasMenu />);
        const btn = queryByTitle("addCanvas");
        fireEvent.click(btn);
        expect(document.getElementById("canvasNaam")).toBeInTheDocument();
    });
})

// Checkt of de menu modal wordt geopend
describe('Check the menu button', () => {
    it('onClick', () => {
        const { queryByTitle } = render(<CanvasMenu />);
        const btn = queryByTitle("menu");
        fireEvent.click(btn);
        expect(document.getElementById("menuId")).toBeInTheDocument();
    });
})