
const CanvasForm = ({ postOrPut, name, setName,
    points, setPoints, map,
    metric1, metric2, metric3,
    values, setValues,
    mapDropdown, metricsDropdown, newsArticlesDropdown,
    newsArticle1, newsArticle2, newsArticle3,
    handleMapDropdown, handleMetricDropdown, handleNewsArticleDropdown,
    openModalHandler
}) => {
    return (
        <>
            {/* Name, points and map */}
            <form className="canvasContent-form" onSubmit={postOrPut}>
                <div className="textarea-rows textarea-rows2">
                    <label id="canvasNaam">Canvas naam</label>
                    <label>Punten</label>
                </div>
                <div className="textarea-rows textarea-rows2">
                    <textarea value={name} onChange={e => setName(e.target.value)} />
                    <textarea value={points} onChange={e => setPoints(e.target.value)} />
                </div>
                <div className="canvas-form-row-label">
                    <label>Kaart</label>
                    <select onChange={e => handleMapDropdown(e)}>
                        <option defaultValue="Kies een kaart">Kies een kaart</option>
                        {mapDropdown.map(map => <option key={map.id} value={map.name}>{map.name}</option>)}
                    </select>
                </div>
                <textarea value={map.name} className="greyed-out" readOnly />
                {/* Metrics */}
                <div className="canvas-form-row-label">
                    <label>Metriek 1</label>
                    <select onChange={e => handleMetricDropdown(e, 1)}>
                        <option defaultValue="Kies een andere metriek">Kies een andere metriek</option>
                        {metricsDropdown.map(metric => <option key={metric.id} value={metric.name}>{metric.name}</option>)}
                    </select>
                </div>
                <div className="canvas-form-row-values">
                    <textarea className="greyed-out" readOnly value={metric1.name} />
                    <textarea value={values.value1} onChange={e => setValues({ ...values, value1: e.target.value })} />
                </div>
                <div className="canvas-form-row-label">
                    <label>Metriek 2</label>
                    <select onChange={e => handleMetricDropdown(e, 2)}>
                        <option defaultValue="Kies een andere metriek">Kies een andere metriek</option>
                        {metricsDropdown.map(metric => <option key={metric.id} value={metric.name}>{metric.name}</option>)}
                    </select>
                </div>
                <div className="canvas-form-row-values">
                    <textarea className="greyed-out" readOnly value={metric2.name} />
                    <textarea value={values.value2} onChange={e => setValues({ ...values, value2: e.target.value })} />
                </div>
                <div className="canvas-form-row-label">
                    <label>Metriek 3</label>
                    <select onChange={e => handleMetricDropdown(e, 3)}>
                        <option defaultValue="Kies een andere metriek">Kies een andere metriek</option>
                        {metricsDropdown.map(metric => <option key={metric.id} value={metric.name}>{metric.name}</option>)}
                    </select>
                </div>
                <div className="canvas-form-row-values">
                    <textarea className="greyed-out" readOnly value={metric3.name} />
                    <textarea value={values.value3} onChange={e => setValues({ ...values, value3: e.target.value })} />
                </div>
                {/* Newsarticles */}
                <div className="canvas-form-row-label">
                    <label>Nieuwsartikel 1</label>
                    <select onChange={e => handleNewsArticleDropdown(e, 1)}>
                        <option defaultValue="Kies een ander nieuwsartikel">Kies een ander nieuwsartikel</option>
                        {newsArticlesDropdown.map(article => <option key={article.id} value={article.title}>{article.title}</option>)}
                    </select>
                </div>
                <textarea className="greyed-out" readOnly value={newsArticle1.title} />
                <div className="canvas-form-row-label">
                    <label>Nieuwsartikel 2</label>
                    <select onChange={e => handleNewsArticleDropdown(e, 2)}>
                        <option defaultValue="Kies een ander nieuwsartikel">Kies een ander nieuwsartikel</option>
                        {newsArticlesDropdown.map(article => <option key={article.id} value={article.title}>{article.title}</option>)}
                    </select>
                </div>
                <textarea className="greyed-out" readOnly value={newsArticle2.title} />
                <div className="canvas-form-row-label">
                    <label>Nieuwsartikel 3</label>
                    <select onChange={e => handleNewsArticleDropdown(e, 3)}>
                        <option defaultValue="Kies een ander nieuwsartikel">Kies een ander nieuwsartikel</option>
                        {newsArticlesDropdown.map(article => <option key={article.id} value={article.title}>{article.title}</option>)}
                    </select>
                </div>
                <textarea className="greyed-out" readOnly value={newsArticle3.title} />
                <div className="div-save-button">
                    <button className="button-delete" onClick={openModalHandler}>Verwijder canvas</button>
                    <input className="button-save" type="submit" value="Opslaan" />
                </div>
            </form>
        </>
    );
}

export default CanvasForm;
