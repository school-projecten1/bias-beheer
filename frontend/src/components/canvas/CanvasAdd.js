import { useState } from "react";
import Backdrop from "../modal/Backdrop";
import Modal from "../modal/Modal";
import CanvasForm from "./CanvasForm";

const CanvasAdd = ({ fetchAllCanvas, map, metrics, newsArticles,
    handleMapDropdown, handleMetricDropdown, handleNewsArticleDropdown,
    mapDropdown, metricsDropdown, newsArticlesDropdown }) => {
    const [name, setName] = useState('');
    const [points, setPoints] = useState('');
    const [values, setValues] = useState({
        value1: '',
        value2: '',
        value3: ''
    });
    const [alertModal, setAlertModal] = useState(null);
    const [emptyFieldModal, setEmptyFieldModal] = useState(null);

    const postCanvas = async (e) => {
        e.preventDefault();
        if (values.value1 === '' || values.value2 === '' || values.value3 === '') {
            setEmptyFieldModal(true);
            return;
        }
        const newCanvas = {
            name: name,
            points: points,
            map: map,
            metrics: [metrics.metric1, metrics.metric2, metrics.metric3],
            value1: values.value1,
            value2: values.value2,
            value3: values.value3,
            newsArticles: [newsArticles.newsArticle1, newsArticles.newsArticle2, newsArticles.newsArticle3]
        };
        setAlertModal(true);
        if (alertModal) {
            await postRequest(newCanvas);
        }
    }

    const postRequest = async (canvas) => {
        await fetch('http://localhost:8080/api/canvas/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(canvas)
        })
        fetchAllCanvas();
    }

    const closeModalHandler = () => {
        setAlertModal(false);
        setEmptyFieldModal(false);
    }

    return (
        <div className="div-content">
            <CanvasForm
                postOrPut={postCanvas} name={name}
                setName={setName} points={points} setPoints={setPoints}
                map={map} metric1={metrics.metric1} metric2={metrics.metric2} metric3={metrics.metric3}
                values={values} setValues={setValues}
                mapDropdown={mapDropdown} metricsDropdown={metricsDropdown} newsArticlesDropdown={newsArticlesDropdown}
                newsArticle1={newsArticles.newsArticle1} newsArticle2={newsArticles.newsArticle2} newsArticle3={newsArticles.newsArticle3}
                handleMapDropdown={handleMapDropdown} handleMetricDropdown={handleMetricDropdown}
                handleNewsArticleDropdown={handleNewsArticleDropdown} />
            {/* Opens the modal for the save button. */}
            {alertModal && <Modal cancelHandler={closeModalHandler} confirmHandler={postCanvas} modalQuestion='Weet je zeker dat je een nieuwe canvas wilt toevoegen?' modalCancelText='Annuleren' modalConfirmText='Opslaan' />}
            {alertModal && <Backdrop cancelModal={closeModalHandler} />}
            {/* Opens the modal when there are empty fields */}
            {emptyFieldModal && <Modal cancelHandler={closeModalHandler} confirmHandler={closeModalHandler} modalQuestion='Vul alle velden in voordat je gaat opslaan!' modalCancelText='Sluiten' modalConfirmText='Velden invullen' />}
            {emptyFieldModal && <Backdrop cancelModal={closeModalHandler} />}
        </div>
    );
}

export default CanvasAdd;
