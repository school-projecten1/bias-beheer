import { useState, useEffect } from "react";
import Backdrop from "../modal/Backdrop";
import checkModal from "../../Functions/checkModal";
import CanvasForm from "./CanvasForm";

const CanvasContent = ({ canvasContent, fetchAllCanvas, map, metrics, setMetrics,
    newsArticles, setNewsArticles,
    handleMapDropdown, handleMetricDropdown, handleNewsArticleDropdown,
    setMap, mapDropdown, metricsDropdown, newsArticlesDropdown }) => {
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [modalSaveOpen, setModalSaveOpen] = useState(false);
    const [name, setName] = useState('');
    const [points, setPoints] = useState('');
    const [values, setValues] = useState({
        value1: '',
        value2: '',
        value3: ''
    });

    useEffect(() => {
        setName(canvasContent.name);
        setPoints(canvasContent.points);
        setMap(canvasContent.map);
        setMetrics({ metric1: canvasContent.metrics[0], metric2: canvasContent.metrics[1], metric3: canvasContent.metrics[2] });
        setValues({value1: canvasContent.value1, value2: canvasContent.value2, value3: canvasContent.value3});
        if (canvasContent.newsArticles.length > 2) {
            setNewsArticles({ newsArticle1: canvasContent.newsArticles[0], newsArticle2: canvasContent.newsArticles[1], newsArticle3: canvasContent.newsArticles[2] });
        } else {
            setNewsArticles({ newsArticle1: canvasContent.newsArticles[0], newsArticle2: canvasContent.newsArticles[1], newsArticle3: {
                id: 0,
                title: '',
                message: '',
                source: '',
                popUp: false
            } });
        }
    }, [canvasContent, setMap, setNewsArticles, setMetrics]);

    const closeModalHandler = () => {
        setModalIsOpen(false);
        setModalSaveOpen(false);
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    const putUpdatedCanvas = async (e) => {
        e.preventDefault();
        const updatedCanvas = {
            name: name,
            id: canvasContent.id,
            points: points,
            map: map,
            metrics: [metrics.metric1, metrics.metric2, metrics.metric3],
            value1: values.value1,
            value2: values.value2,
            value3: values.value3,
            newsArticles: [newsArticles.newsArticle1, newsArticles.newsArticle2, newsArticles.newsArticle3]
        };
        setModalSaveOpen(true);
        if (modalSaveOpen) {
            await putRequest(updatedCanvas);
        }
    }

    const putRequest = async (updatedCanvas) => {
        await fetch(`http://localhost:8080/api/canvas/update/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedCanvas)
        });
        fetchAllCanvas(); // Refreshes the list and loads the buttons
    }

    const deleteCanvas = async () => {
        await fetch(`http://localhost:8080/api/canvas/delete/${canvasContent.id}`, {
            method: 'DELETE'
        });
        fetchAllCanvas();
    }

    return (
        <div className="div-content">
            <CanvasForm
                postOrPut={putUpdatedCanvas} canvasContent={canvasContent} name={name}
                setName={setName} points={points} setPoints={setPoints}
                map={map} metric1={metrics.metric1} metric2={metrics.metric2} metric3={metrics.metric3}
                values={values} setValues={setValues}
                mapDropdown={mapDropdown} metricsDropdown={metricsDropdown} newsArticlesDropdown={newsArticlesDropdown}
                newsArticle1={newsArticles.newsArticle1} newsArticle2={newsArticles.newsArticle2} newsArticle3={newsArticles.newsArticle3}
                handleMapDropdown={handleMapDropdown} handleMetricDropdown={handleMetricDropdown}
                handleNewsArticleDropdown={handleNewsArticleDropdown} openModalHandler={openModalHandler} />
            {(modalIsOpen || modalSaveOpen) && <Backdrop cancelModal={closeModalHandler} />}
            {checkModal(modalIsOpen, modalSaveOpen, closeModalHandler, deleteCanvas, putUpdatedCanvas)}
        </div>
    );
}

export default CanvasContent;
