import { useCallback, useEffect, useState } from "react";
import TimerAdd from "./TimerAdd";
import ButtonList from "../ButtonList";
import TimerContent from "./TimerContent";
import ModalMenu from "../modal/ModalMenu";
import Backdrop from "../modal/Backdrop";
import SearchIcon from '@mui/icons-material/Search';

const TimerMenu = () => {
    const [timerList, setTimerList] = useState([]);
    const [timerContent, setTimerContent] = useState([]);
    const [chosenIndex, setChosenIndex] = useState(0);
    const [newTimer, addTimer] = useState(false);
    const [listForButtons, setListForButtons] = useState([]);
    const [openModalMenu, setOpenModalMenu] = useState(false);
    const [searchInput, setSearchInput] = useState('');

    const fetchAllTimers = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/timer');
        const data = await res.json();
        setTimerList(data);
        setButtons(data);
        setChosenIndex(false);
        addTimer(false);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            fetchAllTimers();
        }
        fetchData();
    }, [fetchAllTimers]);

    const handleTimerClick = (clickedTimerId) => {
        timerList.forEach(timer => {
            if (timer.id === clickedTimerId) {
                addTimer(false);
                setTimerContent(timer);
                setChosenIndex(clickedTimerId);
                setOpenModalMenu(false);
                setSearchInput('');
            }
        });
    }

    const clickAddTimer = () => {
        setChosenIndex(false);
        addTimer(true)
    }

    const setButtons = (data) => {
        const buttonList = [];
        data.forEach(timer => {
            const button = { id: timer.id, name: timer.name }
            buttonList.push(button);
        });
        setListForButtons(buttonList);
    }

    const cancelHandler = () => {
        setOpenModalMenu(false);
        setSearchInput('');
    }

    const openMenu = () => {
        setOpenModalMenu(true);
    }

    const returnFilteredData = (searchValue) => {
        if (searchValue !== '') {
            return timerList.filter((item) => {
                return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
            });
        } else {
            return timerList;
        }
    }

    return (
        <div className="container-menu">
            <div className="btn-group">
                {timerList.length > 0 && <ButtonList list={listForButtons} handleItemClick={handleTimerClick} color={chosenIndex} />}
                <button onClick={clickAddTimer} title="addTimer">+</button>
            </div>
            <div className="container-content">
                {!newTimer && !chosenIndex ? 'Selecteer een timer of maak een nieuwe aan.' : ''}
                {chosenIndex && <TimerContent timerContent={timerContent} fetchAllTimers={fetchAllTimers} />}
                {newTimer ? <TimerAdd fetchAllTimers={fetchAllTimers} /> : ''}
            </div>
            <button className="button-menu" onClick={openMenu} title="menu"><SearchIcon /></button>
            <div>
                <button className="button-add" onClick={clickAddTimer}>+</button>
            </div>
            <div>
                {openModalMenu && <ModalMenu modalQuestion="Maak een keuze uit het menu" cancelHandler={cancelHandler} modalCancelText="Annuleren" handleClick={handleTimerClick} listForButtons={returnFilteredData(searchInput)} listForResults={(e) => setSearchInput(e.target.value)} />}
                {openModalMenu && <Backdrop cancelModal={cancelHandler} />}
            </div>
        </div>
    );
}

export default TimerMenu;