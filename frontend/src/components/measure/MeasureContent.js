import { useState, useEffect } from "react";
import Backdrop from "../modal/Backdrop";
import checkModal from "../../Functions/checkModal";

const MeasureContent = ({ measureContent, fetchAllMeasures }) => {
    const [textMeasureAnswer, setTextMeasureAnswer] = useState('');
    const [points, setPoints] = useState('');
    const [modalIsOpen, setModalIsOpen] = useState(null);
    const [modalSaveOpen, setModalSaveOpen] = useState(null);

    useEffect(() => {
        setTextMeasureAnswer(measureContent.answer);
        setPoints(measureContent.points);
    }, [measureContent]);

    const putUpdatedMeasure = async (e) => {
        e.preventDefault();
        const updatedMeasure = {
            id: measureContent.id,
            answer: textMeasureAnswer,
            points: points
        };
        setModalSaveOpen(true);
        if (modalSaveOpen) {
            await putRequest(updatedMeasure);
        }
    }

    const putRequest = async (updatedMeasure) => {
        await fetch(`http://localhost:8080/api/measure_question/update/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedMeasure)
        });
        fetchAllMeasures();
    }

    const deleteMeasure = async () => {
        await fetch(`http://localhost:8080/api/measure_question/delete/${measureContent.id}`, {
            method: 'DELETE'
        });
        fetchAllMeasures();
    }

    const closeModalHandler = () => {
        setModalIsOpen(false);
        setModalSaveOpen(false);
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    return (
        <div className="div-content">
            <form className="measureContent-form" onSubmit={putUpdatedMeasure}>
                <div className="textarea-rows textarea-rows2">
                    <label>Maatregel antwoord</label>
                    <label>Punten</label>
                </div>
                <div className="textarea-rows textarea-rows2">
                    <textarea value={textMeasureAnswer} onChange={e => setTextMeasureAnswer(e.target.value)}/>
                    <textarea value={points} onChange={e => setPoints(e.target.value)}/>
                </div>
                <div className="div-save-button">
                    <button className="button-delete" onClick={openModalHandler}>Verwijder maatregel</button>
                    <input className="button-save" type="submit" value="Opslaan" />
                </div>
            </form>
            {(modalIsOpen || modalSaveOpen) && <Backdrop cancelModal={closeModalHandler} />}
            {checkModal(modalIsOpen, modalSaveOpen, closeModalHandler, deleteMeasure, putUpdatedMeasure)}
        </div>
    );
}

export default MeasureContent;